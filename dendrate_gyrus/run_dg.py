from dendrate_gyrus.dg import DendrateGyrus
from dendrate_gyrus.ojas_rule import OjasRule


def run_demo(pattern_, flag):
    dg = DendrateGyrus(flag=flag, pattern_list=pattern_)

    for i in range(0,3):
        dg.alpha = 0.25
        dg.run_demo()

        #print(f"Input Layer {i} is: {dg.iplayer}")
        #print(f"Output Layer {i} is: {dg.oplayer}")

    dg.extract_activation_list()
    #print(f"After activation, values are: {dg.oplayer}")

    ojr = OjasRule(dg)
    for i in range(0, 2):
        ojr.activation_function()
        ojr.weight_update()

    ojr.extract_activation_list()

    #print(f"After Finishing Oja, OP Tensor: {ojr.oplayer}")

    ojr_op_layer = ojr.oplayer.numpy()
    return ojr_op_layer

if __name__ == '__main__':
    run_demo()
