from input_generator.binary_input_gen import BinaryInputGenerator

def run_demo():
    bin_ip_gen = BinaryInputGenerator()
    bin_ip_gen.generate_bin_pattern()
    bin_ip_gen.write_to_file()
    print(bin_ip_gen)

if __name__ == '__main__':
    run_demo()