class Config(object):
    APP_NAME: str = "Oja's Rule -- Hippo"
    SECRET_KEY: str = "ThisIsSpartaaa"
    CODER_NAME: str = "Vishwanath"

class OjasConfig(Config):

    OUTPUT_LAYER_NEURONS = 1500
    DILUTION_DG_CA3 = 0.6
    OUTPUT_ACTIVATION_RATIO = 0.15

    ALPHA = 0.25
    BETA = 0.25