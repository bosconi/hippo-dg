from hopfield_network import network, pattern_tools, plot_tools
import numpy
import time
from ec2.run_ec2 import run_demo as ec2_demo
pattern_size = (10, 10)
nr_of_flips=1
nr_patterns=4
nr_neurons= 20 #Number of neurons in CA3
dilution = 0.9


#hopfield_net = network.HopfieldNetwork(nr_neurons= pattern_size**2)
hopfield_net = network.HopfieldNetwork(nr_neurons=nr_neurons, dilution=dilution)
print(hopfield_net.weights.shape)
#factory = pattern_tools.PatternFactory(pattern_size, pattern_size)
factory = pattern_tools.PatternFactory(pattern_length=pattern_size[0], pattern_width=pattern_size[1])

ec2_input = ec2_demo(on_probablity=0.4, pattern_factory=factory, number_of_patterns=nr_patterns,
                     pattern_size=pattern_size)
print(ec2_input)
#pattern_list = factory.create_row_patterns(10)

plot_tools.plot_pattern_list(ec2_input.pattern_list)

overlap_matrix = pattern_tools.compute_overlap_matrix(ec2_input.pattern_list)
plot_tools.plot_overlap_matrix(overlap_matrix)
print("average overlap:",numpy.average(overlap_matrix))

hopfield_net.store_patterns(ec2_input.pattern_list)
print(hopfield_net.weights)
numpy.savetxt('weights.txt', hopfield_net.weights, newline='\n', delimiter=',', fmt='%f')

#print(hopfield_net.weights)
plot_tools.plot_nework_weights(hopfield_net)


noisy_init_state = pattern_tools.flip_n(ec2_input.pattern_list[0], nr_of_flips)
hopfield_net.set_state_from_pattern(noisy_init_state)
hopfield_net.set_dynamics_sign_async()

start_time = time.time()
states = hopfield_net.run_with_monitoring(nr_steps=3)
print("Time to retreive = %s" % (time.time() - start_time))

states_as_patterns = factory.reshape_patterns(states)

plot_tools.plot_state_sequence_and_overlap(states_as_patterns, ec2_input.pattern_list, reference_idx=0, suptitle="Network dynamics")
"""
for j in range(1, 4):
    for i in range(40, 51):
        #noisy_init_state = pattern_tools.flip_n(pattern_list[0], nr_of_flips)
        noisy_init_state = pattern_tools.flip_n(pattern_list[0], i)
        hopfield_net.set_state_from_pattern(noisy_init_state)
        hopfield_net.set_dynamics_sign_async()
        start_time = time.time()
        states = hopfield_net.run_with_monitoring(nr_steps=4)
        time_taken = time.time() - start_time
        print("Run {0} with {1} flips took {2} secs".format(j, i, time_taken))
        states_as_patterns = factory.reshape_patterns(states)

        #print(states_as_patterns[0].shape)
        #print(modified_as_patterns[0].shape)
        plot_tools.plot_state_sequence_and_overlap(states_as_patterns, pattern_list, reference_idx=0, suptitle="Testing with {0} flips and {1} patterns with {2} neurons".format(i, nr_patterns, nr_neurons))
"""