from hopfield_network import plot_tools, pattern_tools, network
import numpy
import time
from input_generator.binary_input_gen import BinaryInputGenerator
#pattern_size =10
#nr_of_flips=1
nr_patterns=8
nr_neurons=40

WEIGHT_FOLDER = 'weights'
FIRST_PATTERN_FOLDER = 'firstpattern'
FIRST_MODIFIED_FOLDER = 'firstmodified'

hopfield_net = network.HopfieldNetwork(nr_neurons)
factory = pattern_tools.PatternFactory(5,8)

#CUSTOM INPUT START
input_pattern = BinaryInputGenerator().read_from_file()
pattern_list = input_pattern.pattern_list
new_pattern_list = []

for pattern in pattern_list:
    pat = pattern.reshape(factory.pattern_length, factory.pattern_width)
    new_pattern_list.append(pat)

pattern_list = new_pattern_list

#CUSTOM INPUT END

#pattern_list = factory.create_random_pattern_list(nr_patterns, on_probability=0.40)
plot_tools.plot_pattern_list(pattern_list)

#print(type(pattern_list))
#print(pattern_list[0].shape)


overlap_matrix = pattern_tools.compute_overlap_matrix(pattern_list)
plot_tools.plot_overlap_matrix(overlap_matrix)

#numpy.savetxt(f'./{FIRST_PATTERN_FOLDER}/100N14P_p0.txt', pattern_list[0], newline='\n', delimiter=',', fmt='%f')



print("average overlap:",numpy.average(overlap_matrix))
print("No of patterns = {0}, No of neurons = {1}".format(nr_patterns, nr_neurons))

modified_list = hopfield_net.store_patterns_with_dg(pattern_list)

print(modified_list)
#numpy.savetxt(f'./{FIRST_MODIFIED_FOLDER}/100N14P_modified.txt', modified_list[0], delimiter=',', newline='\n', fmt='%f')
#numpy.savetxt(f'./{WEIGHT_FOLDER}/100N14P_weights.txt', hopfield_net.weights, newline='\n', delimiter=',', fmt='%f')

plot_tools.plot_nework_weights(hopfield_net)

# j runs with i flips

for j in range(1, 4):
    for i in range(1, 4):
        #noisy_init_state = pattern_tools.flip_n(pattern_list[0], nr_of_flips)
        noisy_init_state = pattern_tools.flip_n(pattern_list[0], i)
        hopfield_net.set_state_from_pattern(noisy_init_state)
        hopfield_net.set_dynamics_sign_async()
        start_time = time.time()
        states = hopfield_net.run_with_monitoring(nr_steps=4)
        time_taken = time.time() - start_time
        print("Run {0} with {1} flips took {2} secs".format(j, i, time_taken))
        states_as_patterns = factory.reshape_patterns(states)
        modified_as_patterns = factory.reshape_patterns(modified_list)
        #print(states_as_patterns[0].shape)
        #print(modified_as_patterns[0].shape)
        plot_tools.plot_state_sequence_and_overlap(states_as_patterns, modified_as_patterns, reference_idx=0, suptitle="Testing with {0} flips and {1} patterns with {2} neurons".format(i, nr_patterns, nr_neurons))
