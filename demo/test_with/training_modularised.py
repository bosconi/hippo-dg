from hopfield_network import pattern_tools, plot_tools, network
from config import HopfieldNetworkConfig as hnconfig
from ec2.ec2 import EntorinhalCortex
from ojas_rule.run_oj import run_oja_from_dg
from ojas_rule.oj import OjasRule
import numpy
import time
import numpy as np
from typing import List
from input_generator.binary_input_gen import BinaryInputGenerator
#TODO: Change the name of file to training.py after finishing the code

#TYPE DEFINITIONS
two_dimesional_vector = np.ndarray

def initialise_hopfield_network()-> network.HopfieldNetwork:

    hopfield_net = network.HopfieldNetwork(nr_neurons=hnconfig.NO_OF_NEURONS)
    return hopfield_net

def pattern_select(parameter: int, pattern_factory) -> List:

    if parameter == 1:
        pattern_list = pattern_factory.create_random_pattern_list(nr_patterns=hnconfig.NO_OF_PATTERN,
                                                                  on_probability=hnconfig.ON_PROBABLITY)
        return pattern_list

    if parameter == 2:
        pattern_list = BinaryInputGenerator().read_from_file().pattern_list
        new_pattern_list = []

        for pattern in pattern_list:
            pattern = pattern.reshape(pattern_factory.pattern_length, pattern_factory.pattern_width)
            new_pattern_list.append(pattern)

        return new_pattern_list

def plot_graphs(pattern_list: List):

    plot_tools.plot_pattern_list(pattern_list=pattern_list)
    overlap_matrix = pattern_tools.compute_overlap_matrix(pattern_list=pattern_list)
    plot_tools.plot_overlap_matrix(overlap_matrix=overlap_matrix)

    print(f"Average Overlap: {numpy.average(overlap_matrix)}")

    print(f"No of patterns = {hnconfig.NO_OF_PATTERN}, No of neurons = {hnconfig.NO_OF_NEURONS}")


def run_program() -> List[two_dimesional_vector]:

    hopfield_net = initialise_hopfield_network()

    pattern_factory = pattern_tools.PatternFactory(hnconfig.PATTERN_FACTORY_LENGTH, hnconfig.PATTERN_FACTORY_WIDTH)

    pattern_list = pattern_factory.create_random_pattern_list(nr_patterns=hnconfig.NO_OF_PATTERN,
                                                           on_probability=hnconfig.ON_PROBABLITY)

    pattern_list = pattern_select(parameter=2, pattern_factory=pattern_factory)
    plot_graphs(pattern_list=pattern_list)

    entorinhal_cortex = EntorinhalCortex(pattern_list)
    entorinhal_cortex.process_pattern_one_by_one()

    modified_list = hopfield_net.store_patterns_with_dg(pattern_list)
    print(modified_list)
    modified_oja_patterns: List = []
    for pattern in modified_list:
        oja_output: OjasRule = run_oja_from_dg(input_layer=pattern)
        modified_oja_patterns.append(oja_output.activation_list_ec_ca3)

    plot_tools.plot_nework_weights(hopfield_network=hopfield_net)

    modified_list = modified_oja_patterns
    del modified_oja_patterns

    for i in range(1,4):
        for j in range(1,4):
            noisy_init_state = pattern_tools.flip_n(pattern_list[0], i)
            hopfield_net.set_state_from_pattern(noisy_init_state)
            hopfield_net.set_dynamics_sign_async()
            start_time = time.time()
            states = hopfield_net.run_with_monitoring(nr_steps=4)
            time_taken = time.time() - start_time
            print(f"Run {i} with {j} flips took {time_taken} steps")
            states_as_patterns = pattern_factory.reshape_patterns(states)
            modified_as_patterns = pattern_factory.reshape_patterns(modified_list)
            title = f"Testing with {j} flips and {hnconfig.NO_OF_PATTERN} patterns with {hnconfig.NO_OF_NEURONS}"
            plot_tools.plot_state_sequence_and_overlap(states_as_patterns, modified_as_patterns,
                                                       reference_idx=0, suptitle=title)
    return 1

if __name__ == '__main__':
    run_program()


