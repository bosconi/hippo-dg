import torch
import random
import math
from typing import List
vector = torch.Tensor
import csv
import os
import numpy as np

class BinaryInputGenerator():

    def __init__(self):

        self.pattern_list= []
        self.no_of_pattern: int = None

    def binary_to_pattern(self, num: int):
        binary = torch.zeros(25)
        counter = 0
        while num!= 0:
            #print(num)
            bit = math.ceil(num % 2)
            binary[counter] = bit
            counter += 1
            print(counter)
            num = int(num/2)

        return binary

    def generate_bin_pattern(self):
        count = 0
        for i in (900,850):
            #print('Pattern 1')
            pattern_1 = self.binary_to_pattern(i)
            for j in (650,701,813):
                #print('Pattern 2')
                pattern_2 = self.binary_to_pattern(j)
                for k in range(256,767,256):
                    #print('Pattern 3')
                    pattern_3 = self.binary_to_pattern(k)
                    for l in (1,100,400,500,651,459):
                        #print('Pattern 4')
                        pattern_4 = self.binary_to_pattern(l)
                        final_pattern = torch.cat((pattern_1, pattern_2, pattern_3, pattern_4), 0)
                        print("Final pattern {0}".format(final_pattern))
                        self.pattern_list.append(final_pattern)

                        if self.no_of_pattern == None:
                            self.no_of_pattern = 1
                        else:
                            self.no_of_pattern += 1
        print('No. of patterns = {0}'.format(self.no_of_pattern))

    def write_to_file(self):
        file_name = "./saved_inputs/current_input.txt"
        file_object = open(file_name, mode='w')
        csv_writer = csv.writer(file_object)
        curr_list = []
        curr_tensor_list = torch.stack(self.pattern_list, dim=1)
        curr_tensor_np = curr_tensor_list.numpy()
        curr_tensor_np = np.transpose(curr_tensor_np, (1,0))
        np.savetxt(fname=file_name, X=curr_tensor_np, fmt="%d")
        file_object.close()

    def read_from_file(self):
        file_name = os.path.join(os.path.dirname(os.path.realpath(__file__)), './saved_inputs/current_input.txt')
        pattern_list = np.loadtxt(fname=file_name, dtype=int)
        pattern_list[pattern_list == 0] = -1
        for i in range(pattern_list.shape[0]):
            pl = np.transpose(pattern_list[i, :])
            self.pattern_list.append(pl)
        print(self.pattern_list)
        return self

if __name__ == '__main__':

    bin_input = BinaryInputGenerator()
    bin_input.generate_bin_pattern()
    bin_input.write_to_file()
