# Hippo - A Hippocampal Simulator

A Simulator to understand the replicate the data storage and retrieval that takes place in Hippocampus.
At the moment, the working of only the Dendrate Gyrus has been completed, along with using Hopfield Networks as a
placeholder for the CA3 region. Future works would expand on each region, simultaneously optimising the completed versions.


## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

The following are some of the dependencies required to expand the project.

```

Python 3.6+ (I've used static type checking in multiple places)
PyTorch v0.4+
Numpy 1.14+
Matplotlib v2.0+

```

### Installing

Creating a Virtual Environment

```

python3 -m venv virtualEnv
source virtualEnv/bin/activate

```

And install all the packages

```

pip install -r requirements.txt

```

## Running the Code
For now, use PyCharm IDE for running the code, we're trying to package it as a library.
```

Run
test_with/training.py for testing with Dendrate Gyrus, and test_without/training.py for testing without DG.

```