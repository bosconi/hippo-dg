import arrow
class GlobalConfig(object):

    APP_NAME = "Vidya"
    RUN_DATE = str(arrow.get())
    RUN_DESCRIPTION = "First run"

class HopfieldNetworkConfig(GlobalConfig):

    NO_OF_FLIPS = 1
    NO_OF_PATTERN = 14
    NO_OF_NEURONS = 100
    ON_PROBABLITY = 0.4

    PATTERN_FACTORY_LENGTH = 10
    PATTERN_FACTORY_WIDTH = 10


    WEIGHT_FOLDER = 'weights'
    FIRST_PATTERN_FOLDER = 'firstpattern'
    FIRST_MODIFIED_FOLDER = 'firstmodified'

class DentateGyrusConfig(GlobalConfig):

    INPUT_LAYER_NEURONS = None
    INPUT_DILATION = 0.5  # Activation within IP Layer of DG

    STANDARD_INPUT = 0

    OUTPUT_LAYER_NEURONS = 1500
    OUTPUT_DILATION = 0.6  # NOT BEING USED
    OUTPUT_ACTIVATION_RATIO = 0.15

    ALPHA = 0.25
    BETA = 0.25

class OjasConfig(GlobalConfig):
    OUTPUT_LAYER_NEURONS = 100
    DILUTION_DG_CA3 = 0.6
    OUTPUT_ACTIVATION_RATIO = 0.15

    BETA = 0.25
    ALPHA = 0.25

class OjasConfigEC2(GlobalConfig):
    OUTPUT_LAYER_NEURONS = 100
    DILUTION_DG_CA3 = 0.6
    OUTPUT_ACTIVATION_RATIO = 0.15

    BETA = 0.25
    ALPHA = 0.25